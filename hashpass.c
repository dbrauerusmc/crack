#include <stdio.h>
#include <string.h>
#include "md5.h"
#include <stdlib.h>

int main(int argc, char *argv[])
{
    if (argc < 2 || argc > 3)
    {
        printf("Usage: %s word_to_hash [output_file]\n", argv[0]);
        exit(1);
    }
    
    // Open first argument for reading
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }    
    
    FILE *out;
    if (argc == 3)
    {
    
        out = fopen(argv[2], "w");
        if (!out)
        {
            printf("can't open %s for writing\n", argv[2]);
            exit(2);   
        }
    }
    else
        out = stdout;
        
    // Read all lines and print them as we go
    char line[100];
    while(fgets(line, 100, fp) != NULL)
    {
        for(int i = 0; i < strlen(line); i++)
        {
            if (line[i] == '\n')
            {
                line[i] = '\0';
            }
        }

            
        if (argc == 3)
        {
            
            char *hash = md5(line, strlen(line));
            fprintf(out, "%s\n", hash);
            free(hash);
           // fprintf(out, "%s", line);
        }
        else
        {
            printf("%s", line);
        }
    }
    
    // Close file
    fclose(fp);
    if (argc == 3) fclose(out);        
        
    

}